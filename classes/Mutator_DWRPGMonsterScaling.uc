class Mutator_DWRPGMonsterScaling extends MutUT2004RPG
	config(UT2004RPG);
	
///////////////////////////////////////////////////////////////////////////////////
	
// Internal - Current Invasion Wave
var int CurrentInvasionWave;

// Config - Use the Average Player Level for MonsterLevel instead of Lowest Level Player
var config bool bUseAveragePlayerLevel;

// Internal - Average Player Level
var float AveragePlayerLevel;

// Internal - Total Player Count (includes dead players)
var int TotalPlayerCount;

// Config - Use a Per-Wave MonsterLevel Maximum
var config bool bUsePerWaveMonsterLevelMaximum;

// Config - Defined Per-Wave MonsterLevel Maximums
var config array<int> PerWaveMonsterLevelMaximum;

// Config - Absolute MonsterLevel Maximum
var config int MonsterLevelMaximum;

// Config - Use Low PlayerCount MonsterLevel Scaling
var config bool bUseLowPlayerCountMonsterLevelScaling;

// Config - Use Low PlayerCount MonsterLevel Scaling, but do so after applying PerWave MonsterLevel Maximums
var config bool bUseLowPlayerCountMonsterLevelScalingOnPerWaveMaximum; // Apply Low Player Count MonsterLevel Scaling after PerWave MonsterLevel Maximum

// Config - Once this number is reached, LowPlayerCountMonsterLevelScaling will no longer be relevant
var config int LowPlayerCountThresholdMax;

// Config - Scaling MonsterLevel by this Percentage (e.g. 0.8 means the maximum MonsterLevel with 1 player will be 80% of normal MonsterLevel)
var config float LowPlayerCountMonsterLevelScalingMax;

// Internal - Current Calculated value for Low PlayerCount MonsterLevel Scaling
var float LowPlayerCountMonsterLevelScaling;


var localized string PropsDisplayText[9];
var localized string PropsDescText[9];
var localized string PropsExtras;

///////////////////////////////////////////////////////////////////////////////////

function PostBeginPlay()
{
	local RPGRules G;
	local int x;
	local Pickup P;
	local RPGPlayerDataObject DataObject;
	local array<string> PlayerNames;
	local TCPNetDriver NetDriver;
	local string DownloadManagers;

	//update version
	if (Version <= 20)
	{
		if (Version <= 12)
		{
			// if we're using the default EXP table from version 1.2, update it
			if (Levels.length == 29 && Levels[28] == 150 && InfiniteReqEXPOp == 0 && InfiniteReqEXPValue == 0)
			{
				InfiniteReqEXPValue = 5;
			}
			// add Healing magic weapon to the list
			WeaponModifiers.Insert(0, 1);
			WeaponModifiers[0].WeaponClass = class<RPGWeapon>(DynamicLoadObject("UT2004RPG.RW_Healing", class'Class'));
			WeaponModifiers[0].Chance = 1;
		}

		class'RPGArtifactManager'.static.UpdateArtifactList();

		Version = RPG_VERSION;
		SaveConfig();
	}

	G = spawn(class'RPGMonsterScalingRules');
	G.RPGMut = self;
	G.PointsPerLevel = PointsPerLevel;
	G.LevelDiffExpGainDiv = LevelDiffExpGainDiv;
	
	//RPGRules needs to be first in the list for compatibility with some other mutators (like UDamage Reward)
	if (Level.Game.GameRulesModifiers != None)
	{
		G.NextGameRules = Level.Game.GameRulesModifiers;
	}
	Level.Game.GameRulesModifiers = G;

	if (bReset)
	{
		//load em all up, and delete them one by one
		PlayerNames = class'RPGPlayerDataObject'.static.GetPerObjectNames("UT2004RPG",, 1000000);
		for (x = 0; x < PlayerNames.length; x++)
		{
			DataObject = new(None, PlayerNames[x]) class'RPGPlayerDataObject';
			DataObject.ClearConfig();
			//bleh, this sucks, what a waste of memory
			//if only ClearConfig() actually cleared the properties of the object instance...
			DataObject = new(None, PlayerNames[x]) class'RPGPlayerDataObject';
		}

		bReset = false;
		SaveConfig();
	}

	for (x = 0; x < WeaponModifiers.length; x++)
	{
		TotalModifierChance += WeaponModifiers[x].Chance;
	}

	Spawn(class'RPGArtifactManager');

	if (SaveDuringGameInterval > 0.0 && !bIronmanMode)
	{
		SetTimer(SaveDuringGameInterval, true);
	}

	if (StartingLevel < 1)
	{
		StartingLevel = 1;
		SaveConfig();
	}

	BotSpendAmount = PointsPerLevel * 3;

	//HACK - if another mutator played with the weapon pickups in *BeginPlay() (like Random Weapon Swap does)
	//we won't get CheckRelevance() calls on those pickups, so find any such pickups here and force it
	foreach DynamicActors(class'Pickup', P)
	{
		if (P.bScriptInitialized && !P.bGameRelevant && !CheckRelevance(P))
		{
			P.Destroy();
		}
	}

	//remove any disallowed abilities
	for (x = 0; x < Abilities.length; x++)
	{
		if (Abilities[x] == None)
		{
			Abilities.Remove(x, 1);
			SaveConfig();
			x--;
		}
		else if (!Abilities[x].static.AbilityIsAllowed(Level.Game, self))
		{
			RemovedAbilities[RemovedAbilities.length] = Abilities[x];
			Abilities.Remove(x, 1);
			SaveConfig();
			x--;
		}
	}
	
	//See if any abilities that weren't allowed last game are allowed this time
	//(so user doesn't have to fix ability list when switching gametypes/mutators a lot)
	for (x = 0; x < RemovedAbilities.length; x++)
	{
		if (RemovedAbilities[x].static.AbilityIsAllowed(Level.Game, self))
		{
			Abilities[Abilities.length] = RemovedAbilities[x];
			RemovedAbilities.Remove(x, 1);
			SaveConfig();
			x--;
		}
	}

	// set up an extra download manager that we'll override later with the official UT2004RPG redirect
	if (Level.NetMode != NM_StandAlone && bUseOfficialRedirect)
	{
		foreach AllObjects(class'TCPNetDriver', NetDriver)
		{
			DownloadManagers = NetDriver.GetPropertyText("DownloadManagers");
			NetDriver.SetPropertyText("DownloadManagers", "(\"IpDrv.HTTPDownload\"," $ Right(DownloadManagers, Len(DownloadManagers) - 1));
		}
	}

	Super(Mutator).PostBeginPlay();
}

//find who is now the lowest level player
function FindCurrentLowestLevelPlayer()
{
	local Controller C;
	local Inventory Inv;
	local RPGStatsInv RPGInv;
	local float SumPlayerLevel;
	local int NumberOfLivingPlayers;
	
	local Invasion InvasionGame;
	local bool bRecalculateLowPlayerCount;
	local float LowPlayerCountPercentage;
	
	TotalPlayerCount = 0;
	SumPlayerLevel = 0;
	NumberOfPlayers = 0;
	CurrentLowestLevelPlayer = None;
	
	
	if (Level.Game.IsA('Invasion'))
	{
	    InvasionGame = Invasion(Level.Game);
	    if (InvasionGame != None && InvasionGame.WaveNum != CurrentInvasionWave)
		{
		   CurrentInvasionWave = InvasionGame.WaveNum;
		   bRecalculateLowPlayerCount = true;
		}
	}
	
	for (C = Level.ControllerList; C != None; C = C.NextController)
	{
		if (C.bIsPlayer)
		{
		    TotalPlayerCount++;
		}

		if (C.bIsPlayer && C.PlayerReplicationInfo != None && (C.IsA('PlayerController')))
		{
		    if (!C.PlayerReplicationInfo.bOutOfLives)
			{
				for (Inv = C.Inventory; Inv != None; Inv = Inv.Inventory)
				{
					if (RPGStatsInv(Inv) != None)
					{
						RPGInv = RPGStatsInv(Inv);
						if (RPGInv != None)
						{
							if ( CurrentLowestLevelPlayer == None || RPGInv.DataObject.Level < CurrentLowestLevelPlayer.Level )
							{					
								CurrentLowestLevelPlayer = RPGStatsInv(Inv).DataObject;
							}
						
							// We want to also calculate our Average Player Level, regardless
							NumberOfLivingPlayers++;
							SumPlayerLevel += RPGInv.DataObject.Level;
						}
					}
				}
			}
		}
	}
	
	// Find Average Player Level
	AveragePlayerLevel = Max(StartingLevel, (SumPlayerLevel / Max(1,NumberOfPlayers)) );
	
	if (bRecalculateLowPlayerCount)
	{
	    if (TotalPlayerCount >= LowPlayerCountThresholdMax)
		{
		    LowPlayerCountMonsterLevelScaling = 1.f;
		}
		else
		{
		    LowPlayerCountPercentage = float(TotalPlayerCount) / float(LowPlayerCountThresholdMax);
			LowPlayerCountMonsterLevelScaling = Lerp(LowPlayerCountPercentage, LowPlayerCountMonsterLevelScalingMax, 1.f, true);
		}
	}
}

function GetServerDetails(out GameInfo.ServerResponseLine ServerState)
{
	local int i, NumPlayers;
	local float AvgLevel;
	local Controller C;
	local Inventory Inv;

	Super.GetServerDetails(ServerState);

	i = ServerState.ServerInfo.Length;

	ServerState.ServerInfo.Length = i+1;
	ServerState.ServerInfo[i].Key = "DWRPG Monster Scaling Version";
	ServerState.ServerInfo[i++].Value = ""$(RPG_VERSION / 10)$"."$int(RPG_VERSION % 10);


	//find average level of players currently on server
	if (!Level.Game.bGameRestarted)
	{
		for (C = Level.ControllerList; C != None; C = C.NextController)
		{
			if (C.bIsPlayer && (C.IsA('PlayerController')))
			{
				for (Inv = C.Inventory; Inv != None; Inv = Inv.Inventory)
					if (Inv.IsA('RPGStatsInv'))
					{
						AvgLevel += RPGStatsInv(Inv).DataObject.Level;
						NumPlayers++;
					}
			}
		}
		if (NumPlayers > 0)
			AvgLevel = AvgLevel / NumPlayers;

		ServerState.ServerInfo.Length = i+1;
		ServerState.ServerInfo[i].Key = "Current Avg. Level";
		ServerState.ServerInfo[i++].Value = ""$AvgLevel;
	}

	if (Level.Game.IsA('Invasion'))
	{
		ServerState.ServerInfo.Length = i+1;
		
		ServerState.ServerInfo[i].Key = "Use Per Wave Monster Level Maximum";
		ServerState.ServerInfo[i++].Value = string(bUsePerWaveMonsterLevelMaximum);
		
		if (bUsePerWaveMonsterLevelMaximum)
		{
			ServerState.ServerInfo.Length = i+1;
			ServerState.ServerInfo[i].Key = "Monster Adjustment Factor";
			ServerState.ServerInfo[i++].Value = string(InvasionAutoAdjustFactor);
		}
	}
}


static function FillPlayInfo(PlayInfo PlayInfo)
{
	local int i;

	Super.FillPlayInfo(PlayInfo);
	
	// Add MonsterScaling Properties
	PlayInfo.AddSetting("UT2004RPG", "MonsterScaling", default.PropsDisplayText[i++], 4, 10, "Check",,,, true);
	
}

static function string GetDescriptionText(string PropName)
{
	switch (PropName)
	{
		//case "SaveDuringGameInterval":	return default.PropsDescText[0];
		//case "StartingLevel":		return default.PropsDescText[1];
		//case "bAutoAdjustInvasionLevel":return default.PropsDescText[10];
		//case "InvasionAutoAdjustFactor":return default.PropsDescText[11];
	}
}

defaultproperties
{
     Version=10

     InvasionAutoAdjustFactor=0.300000
	 
	 PropsDisplayText(10)="Auto Adjust Invasion Monster Level"
     PropsDisplayText(11)="Monster Adjustment Factor"
	 
     PropsDescText(0)="During the game, all data will be saved every this many seconds."
     PropsDescText(1)="New players start at this Level."
     PropsDescText(2)="The number of stat points earned from a levelup."
     PropsDescText(3)="Lower values = more exp when killing someone of higher level."
     PropsDescText(4)="The EXP gained for winning a match."
     PropsDescText(5)="If checked, bots' data is not saved and instead they are simply given a level near that of the human player(s)."
     PropsDescText(6)="If checked, player data will be reset before the next match begins."
     PropsDescText(7)="Chance of any given weapon having magical properties."
     PropsDescText(8)="If checked, weapons given to players when they spawn can have magical properties."
     PropsDescText(9)="If checked, magical weapons will always be identified."
     PropsDescText(10)="If checked, Invasion monsters' level will be adjusted based on the lowest level player."
     PropsDescText(11)="Invasion monsters will be adjusted based on this fraction of the weakest player's level."
     PropsDescText(12)="The maximum number of levelup particle effects that can be spawned on a character at once."
     PropsDescText(13)="Limit on how high stats can go. Values less than 0 mean no limit. The stats are: 1: Weapon Speed 2: Health Bonus 3: Max Adrenaline 4: Damage Bonus 5: Damage Reduction 6: Max Ammo Bonus"
     PropsDescText(14)="Allows you to make the EXP required for the next level always increase, no matter how high a level you get. This option controls how it increases."
     PropsDescText(15)="Allows you to make the EXP required for the next level always increase, no matter how high a level you get. This option is the value for the previous option's operation."
     PropsDescText(18)="If checked, only the winning player or team's data is saved - the losers lose the experience they gained that match."
     PropsDescText(19)="If checked, the server will redirect clients to a special official redirect server for UT2004RPG files (all other files will continue to use the normal redirect server, if any)"
     PropsExtras="0;Add Specified Value;1;Add Specified Percent"
	 
     bAddToServerPackages=True
     GroupName="RPG"
     FriendlyName="DW RPG Monster Scaling"
     Description="DWRPG Monster Scaling Enhanced Behavior"
     bAlwaysRelevant=True
     RemoteRole=ROLE_SimulatedProxy
}
