class RPGMonsterScalingRules extends RPGRules;

var Mutator_DWRPGMonsterScaling RPGMonsterScalingMutator;


function ScoreKill(Controller Killer, Controller Killed)
{
	local RPGPlayerDataObject KillerData, KilledData;
	local int x, LevelDifference;
	local Inventory Inv, NextInv;
	local RPGStatsInv StatsInv, KillerStatsInv;
	local vector TossVel, U, V, W;
	local class<Weapon> LastWeapon;

	if (Killed == None)
	{
		Super.ScoreKill(Killer, Killed);
		return;
	}

	//make killed pawn drop any artifacts he's got
	if (Killed.Pawn != None)
	{
		Inv = Killed.Pawn.Inventory;
		while (Inv != None)
		{
			NextInv = Inv.Inventory;
			if (RPGArtifact(Inv) != None)
			{
				TossVel = Vector(Killed.Pawn.GetViewRotation());
				TossVel = TossVel * ((Killed.Pawn.Velocity Dot TossVel) + 500) + Vect(0,0,200);
				TossVel += VRand() * (100 + Rand(250));
				Inv.Velocity = TossVel;
				Killed.Pawn.GetAxes(Killed.Pawn.Rotation, U, V, W);
				Inv.DropFrom(Killed.Pawn.Location + 0.8 * Killed.Pawn.CollisionRadius * U - 0.5 * Killed.Pawn.CollisionRadius * V);
			}
			Inv = NextInv;
		}
	}

	Super.ScoreKill(Killer, Killed);

	// if this player is now out of the game, find the lowest level player that remains
	if (Killed.PlayerReplicationInfo != None && Killed.PlayerReplicationInfo.bOutOfLives)
		RPGMut.FindCurrentLowestLevelPlayer();

	if (Killer == None)
		return;

	//EXP for killing monsters and nonplayer AI vehicles/turrets
	//note: most monster EXP is awarded in NetDamage(); this just notifies abilities and awards an extra 1 EXP
	//to make sure the killer got at least 1 total (plus it's an easy way to know who got the final blow)
	if (Monster(Killed.Pawn) != None || TurretController(Killed) != None)
	{
		StatsInv = GetStatsInvFor(Killer);
		if (StatsInv != None)
		{
			KillerData = StatsInv.DataObject;
			for (x = 0; x < KillerData.Abilities.length; x++)
				KillerData.Abilities[x].static.ScoreKill(Killer, Killed, true, KillerData.AbilityLevels[x]);
			if (Killed.IsA('FriendlyMonsterController'))
			{
				// summoned monsters don't do EXP by damage so award full scoring value
				ShareExperience(StatsInv, float(Monster(Killed.Pawn).ScoringValue));
			}
			else
			{
				ShareExperience(StatsInv, 1.0);
			}
		}
		return;
	}

	if ( Killer == Killed || !Killed.bIsPlayer || !Killer.bIsPlayer
	     || (Killer.PlayerReplicationInfo != None && Killer.PlayerReplicationInfo.Team != None && Killed.PlayerReplicationInfo != None && Killer.PlayerReplicationInfo.Team == Killed.PlayerReplicationInfo.Team) )
		return;

	// if Killed was spawnkilled, no EXP
	if (Killed.Pawn != None && Level.TimeSeconds - Killed.Pawn.LastStartTime < 5)
	{
		LastWeapon = Killed.GetLastWeapon();
		if ( LastWeapon != None && ( LastWeapon.Name == 'AssaultRifle' || LastWeapon.Name == 'ShieldGun'
					     || LastWeapon == Level.Game.BaseMutator.GetDefaultWeapon() ) )
		{
			return;
		}
	}

	//get data
	KillerStatsInv = GetStatsInvFor(Killer);
	if (KillerStatsInv == None)
	{
		Log("KillerData not found for "$Killer.GetHumanReadableName());
		return;
	}
	KillerData = KillerStatsInv.DataObject;

	StatsInv = GetStatsInvFor(Killed);
	if (StatsInv == None)
	{
		Log("KilledData not found for "$Killed.GetHumanReadableName());
		return;
	}
	KilledData = StatsInv.DataObject;

	for (x = 0; x < KillerData.Abilities.length; x++)
		KillerData.Abilities[x].static.ScoreKill(Killer, Killed, true, KillerData.AbilityLevels[x]);
	for (x = 0; x < KilledData.Abilities.length; x++)
		KilledData.Abilities[x].static.ScoreKill(Killer, Killed, false, KilledData.AbilityLevels[x]);

	LevelDifference = Max(0, KilledData.Level - KillerData.Level);
	if (LevelDifference > 0)
		LevelDifference = int(float(LevelDifference*LevelDifference) / LevelDiffExpGainDiv);
	//cap gained exp to enough to get to Killed's level
	if (KilledData.Level - KillerData.Level > 0 && LevelDifference > (KilledData.Level - KillerData.Level) * KilledData.NeededExp)
		LevelDifference = (KilledData.Level - KillerData.Level) * KilledData.NeededExp;
	ShareExperience(KillerStatsInv, 1.0 + LevelDifference);

	//bonus experience for multikills
	if (UnrealPlayer(Killer) != None && UnrealPlayer(Killer).MultiKillLevel > 0)
		KillerData.Experience += Min(Square(float(UnrealPlayer(Killer).MultiKillLevel)), 100);
	else if (AIController(Killer) != None && Killer.Pawn != None && Killer.Pawn.Inventory != None)
		Killer.Pawn.Inventory.OwnerEvent('RPGScoreKill');	//hack to record multikills for bots (handled by RPGStatsInv)

	//bonus experience for sprees
	if (Killer.Pawn != None && Killer.Pawn.GetSpree() % 5 == 0)
		KillerData.Experience += int(Square(float(Killer.Pawn.GetSpree() / 5 + 1)));

	//bonus experience for ending someone else's spree
	if (Killed.Pawn != None && Killed.Pawn.GetSpree() > 4)
		KillerData.Experience += Killed.Pawn.GetSpree() * 2 / 5;

	//bonus experience for first blood
	if (!bAwardedFirstBlood && TeamPlayerReplicationInfo(Killer.PlayerReplicationInfo) != None && TeamPlayerReplicationInfo(Killer.PlayerReplicationInfo).bFirstBlood)
	{
		KillerData.Experience += 2 * Max(KilledData.Level - KillerData.Level, 1);
		bAwardedFirstBlood = true;
	}

	//level up
	RPGMut.CheckLevelUp(KillerData, Killer.PlayerReplicationInfo);
}


function int NetDamage(int OriginalDamage, int Damage, pawn injured, pawn instigatedBy, vector HitLocation, out vector Momentum, class<DamageType> DamageType)
{
	local RPGPlayerDataObject InjuredData, InstigatedData;
	local RPGStatsInv InjuredStatsInv, InstigatedStatsInv;
	local int x, MonsterLevel;
	local FriendlyMonsterController C;
	local bool bZeroDamage;
	local float CurrentInvasionWave;
	local float LowPlayerCountMonsterLevelScalingPercent;

	if (injured == None || instigatedBy == None || injured.Controller == None || instigatedBy.Controller == None)
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);

	C = FriendlyMonsterController(injured.Controller);
	if (C != None && C.Master != None)
	{
		if (C.Master == instigatedBy.Controller)
			Damage = OriginalDamage;
		else if (C.SameTeamAs(instigatedBy.Controller))
			Damage *= TeamGame(Level.Game).FriendlyFireScale;
	}

	// get instigatedBy's RPGStatsInv here so if we bail early we can still give exp for any damage vs monsters
	InstigatedStatsInv = GetStatsInvFor(instigatedBy.Controller);

	if (DamageType.default.bSuperWeapon || Damage >= 1000)
	{
		//if this is weapon damage and the player doing the damage has an RPGWeapon, let it modify the damage
		if (ClassIsChildOf(DamageType, class'WeaponDamageType') && RPGWeapon(InstigatedBy.Weapon) != None)
			RPGWeapon(InstigatedBy.Weapon).NewAdjustTargetDamage(Damage, OriginalDamage, Injured, HitLocation, Momentum, DamageType);
		AwardEXPForDamage(instigatedBy.Controller, InstigatedStatsInv, injured, Damage);
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
	}
	else if (Monster(injured) != None && FriendlyMonsterController(injured.Controller) == None && Monster(instigatedBy) != None && FriendlyMonsterController(instigatedBy.Controller) == None)
	{
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
	}

	if (Damage <= 0)
	{
		Damage = Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
		if (Damage < 0)
		{
			return Damage;
		}
		else if (Damage == 0) //for zero damage, still process abilities/magic weapons so effects relying on hits instead of damage still work
		{
			bZeroDamage = true;
		}
	}

	//get data
	if (InstigatedStatsInv != None)
	{
		InstigatedData = InstigatedStatsInv.DataObject;
	}

	InjuredStatsInv = GetStatsInvFor(injured.Controller);
	if (InjuredStatsInv != None)
		InjuredData = InjuredStatsInv.DataObject;

	if (InstigatedData == None || InjuredData == None)
	{
		if (Level.Game.IsA('Invasion'))
		{
		    CurrentInvasionWave = Invasion(Level.Game).WaveNum;
			MonsterLevel = (CurrentInvasionWave + 1) * 2;
			if (RPGMut.bAutoAdjustInvasionLevel && RPGMut.CurrentLowestLevelPlayer != None)
			{
			    MonsterLevel += Max(0, RPGMut.CurrentLowestLevelPlayer.Level * RPGMut.InvasionAutoAdjustFactor);
				
				if (RPGMonsterScalingMutator != None)
				{
				    // Our New Behavior
					if (RPGMonsterScalingMutator.bUseAveragePlayerLevel)
					{
					    MonsterLevel = (CurrentInvasionWave + 1) * 2;
						MonsterLevel += Max(0, RPGMonsterScalingMutator.AveragePlayerLevel * RPGMut.InvasionAutoAdjustFactor);
					}
					
					if (RPGMonsterScalingMutator.bUseLowPlayerCountMonsterLevelScaling)
					{
						LowPlayerCountMonsterLevelScalingPercent = Min(1.f, float(RPGMonsterScalingMutator.TotalPlayerCount) / float(RPGMonsterScalingMutator.LowPlayerCountThresholdMax));
					    MonsterLevel *= (RPGMonsterScalingMutator.LowPlayerCountMonsterLevelScaling * LowPlayerCountMonsterLevelScalingPercent);
					}
					
					if (RPGMonsterScalingMutator.bUsePerWaveMonsterLevelMaximum)
					{
						if (RPGMonsterScalingMutator.bUseLowPlayerCountMonsterLevelScalingOnPerWaveMaximum)
					    {
						    MonsterLevel = Min(MonsterLevel, RPGMonsterScalingMutator.PerWaveMonsterLevelMaximum[CurrentInvasionWave - 1];
						    LowPlayerCountMonsterLevelScalingPercent = Min(1.f, float(RPGMonsterScalingMutator.TotalPlayerCount) / float(RPGMonsterScalingMutator.LowPlayerCountThresholdMax));
					        MonsterLevel *= (RPGMonsterScalingMutator.LowPlayerCountMonsterLevelScaling * LowPlayerCountMonsterLevelScalingPercent);
					    }
						else
						{
						    MonsterLevel = Min(MonsterLevel, RPGMonsterScalingMutator.PerWaveMonsterLevelMaximum[CurrentInvasionWave - 1];
						}
					}
					
					// Cap it all off
				    MonsterLevel = Min(MonsterLevel, RPGMonsterScalingMutator.MonsterLevelMaximum);
				}
			}
				
		}
		else if (RPGMut.CurrentLowestLevelPlayer != None)
		{
			MonsterLevel = RPGMut.CurrentLowestLevelPlayer.Level;
		}
		else
		{
			MonsterLevel = 1;
		}
		
		if ( InstigatedData == None && ( (instigatedBy.IsA('Monster') && !instigatedBy.Controller.IsA('FriendlyMonsterController'))
						 || TurretController(instigatedBy.Controller) != None ) )
		{
			InstigatedData = RPGPlayerDataObject(Level.ObjectPool.AllocateObject(class'RPGPlayerDataObject'));
			InstigatedData.Attack = MonsterLevel / 2 * PointsPerLevel;
			InstigatedData.Defense = InstigatedData.Attack;
			InstigatedData.Level = MonsterLevel;
		}
		if ( InjuredData == None && ( (injured.IsA('Monster') && !injured.Controller.IsA('FriendlyMonsterController'))
					      || TurretController(injured.Controller) != None ) )
		{
			InjuredData = RPGPlayerDataObject(Level.ObjectPool.AllocateObject(class'RPGPlayerDataObject'));
			InjuredData.Attack = MonsterLevel / 2 * PointsPerLevel;
			InjuredData.Defense = InjuredData.Attack;
			InjuredData.Level = MonsterLevel;
		}
	}

	if (InstigatedData == None)
	{
		//This should never happen
		Log("InstigatedData not found for "$instigatedBy.GetHumanReadableName());
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
	}
	if (InjuredData == None)
	{
		//This should never happen
		Log("InjuredData not found for "$injured.GetHumanReadableName());
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
	}

	//headshot bonus EXP
	if (DamageType.Name == 'DamTypeSniperHeadShot' && InstigatedStatsInv != None && !instigatedBy.Controller.SameTeamAs(injured.Controller))
	{
		InstigatedData.Experience++;
		RPGMut.CheckLevelUp(InstigatedData, InstigatedBy.PlayerReplicationInfo);
	}

	Damage += int((float(Damage) * (1.0 + float(InstigatedData.Attack) * 0.005)) - (float(Damage) * (1.0 + float(InjuredData.Defense) * 0.005)));

	if (Damage < 1 && !bZeroDamage)
		Damage = 1;

	//if this is weapon damage and the player doing the damage has an RPGWeapon, let it modify the damage
	if (ClassIsChildOf(DamageType, class'WeaponDamageType') && RPGWeapon(InstigatedBy.Weapon) != None)
		RPGWeapon(InstigatedBy.Weapon).NewAdjustTargetDamage(Damage, OriginalDamage, Injured, HitLocation, Momentum, DamageType);

	//Allow Abilities to react to damage
	if (InstigatedStatsInv != None)
	{
		for (x = 0; x < InstigatedData.Abilities.length; x++)
			InstigatedData.Abilities[x].static.HandleDamage(Damage, injured, instigatedBy, Momentum, DamageType, true, InstigatedData.AbilityLevels[x]);
	}
	else
		Level.ObjectPool.FreeObject(InstigatedData);
	if (InjuredStatsInv != None)
	{
		for (x = 0; x < InjuredData.Abilities.length; x++)
			InjuredData.Abilities[x].static.HandleDamage(Damage, injured, instigatedBy, Momentum, DamageType, false, InjuredData.AbilityLevels[x]);
	}
	else
		Level.ObjectPool.FreeObject(InjuredData);

	if (bZeroDamage)
	{
		return 0;
	}
	else
	{
		if (InstigatedBy.HasUDamage())
		{
			//UDamage is applied after this function so add it in to get the real amount of damage that will be done
			AwardEXPForDamage(instigatedBy.Controller, InstigatedStatsInv, injured, Damage * 2);
		}
		else
		{
			AwardEXPForDamage(instigatedBy.Controller, InstigatedStatsInv, injured, Damage);
		}
		return Super.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);
	}
}


defaultproperties
{

}
